<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/estilo.css">
    <title>Todo sobre Fútbol</title>
</head>
<body>

<header id="cabecera">
		<?php					
			include 'include/cabecera.php';					
		?>
</header>

<section id="contenido">
        <?php					
			include 'include/contenido.php';					
		?>
</section>

 <footer id="pie">
        <?php					
			include 'include/pie.php';					
		?>   
 </footer>    

</body>
</html>

