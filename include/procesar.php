<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/estilo.css">
    <title>Procesado</title>
</head>
<body>
    
<?php
$nombre = $_REQUEST["nombre"];
$liga = $_REQUEST["liga"];
$jugador = $_REQUEST["jugador"];
$equipo = $_REQUEST["equipo"];
$color = $_POST["color"];
$partidos = $_REQUEST["partidos"];
$puntuar = $_REQUEST["puntuar"];
$puntuarEstrella = "";
$comentario = $_REQUEST["comentario"];
$portero = false;
$defensa = false;
$mediocentro = false;
$delantero = false;
$posicionesnum = 0;
$porteroImg = "";
$defensaImg = "";
$mediocentroImg = "";
$delanteroImg = "";
$posiciones = "";
$fecha = date("d-m-Y H:i:s");

if ($color == "verde") {
    echo "<body style='background-color:green'>";
}
if ($color == "amarillo") {
    echo "<body style='background-color:yellow'>";
}
if ($color == "rojo") {
    echo "<body style='background-color:red'>";
}
if ($color == "azul") {
    echo "<body style='background-color:blue'>";
}

if (isset($_REQUEST["portero"])) {
    $portero = true;
    $posicionesnum ++;
    $porteroImg = "<img src='../img/portero.jpg' alt='' width='200px'>";
}

if (isset($_REQUEST["defensa"])) {
    $defensa = true;
    $posicionesnum ++;
    $defensaImg = "<img src='../img/defensa.jpg' alt='' width='200px'>";
}

if (isset($_REQUEST["mediocentro"])) {
    $mediocentro = true;
    $posicionesnum ++;
    $mediocentroImg = "<img src='../img/mediocentro.jpg' alt='' width='200px'>";
}

if (isset($_REQUEST["delantero"])) {
    $delantero = true;
    $posicionesnum ++;
    $delanteroImg = "<img src='../img/delantero.jpg' alt='' width='200px'>";
}

if ($posicionesnum == 0) {
    $posiciones = "No ha escogido ninguna posición.";
}

for ($i=0; $i < $puntuar; $i++) {
    $puntuarEstrella = $puntuarEstrella. '<img src="../img/star.svg" alt="" width="50px">';
}

if ($comentario == "") {
    $comentario = "No has comentado nada.";
}

echo "

<div id='bloque'>
<p>Nombre y Apeliidos: $nombre</p>
</div>

<div id='bloque'>
<p>La liga que más te gusta es la... $liga</p>
</div>

<div id='bloque'>
<p>Tu jugador favorito para ganar el balón de oro es... <br><br><img src='../img/". $jugador .".jpg' alt='' width='400px'></p>
</div>

<div id='bloque'>
<p>Tu equipo favorito para ganar la Champions es el... $equipo</p>
</div>

<div id='bloque'>
<p>Número de partidos que ves a la semana: $partidos</p>
</div>

<div id='bloque'>
<p>$puntuarEstrella</p>
</div>

<div id='bloque'>
<p>Tu opinión es: $comentario</p>
</div>

<div id='bloque'>
<p id='posicion'>$porteroImg</p>
<p id='posicion'>$defensaImg</p>
<p id='posicion'>$mediocentroImg</p>
<p id='posicion'>$delanteroImg</p>
<p>$posiciones</p>
</div>

<div id='bloque'>
<p>$fecha</p>
</div>

";


?>

</body>
</html>
