<form id="formulario" action="include/procesar.php" method="post">

    <label for="nombre">- Nombre y Apellidos</label>
    <input name="nombre" type="text" required>

    <br><br>
    <label for="tipo">- Escoge tu liga preferida:</label> <br>
    <input name="liga" type="radio" value="La Liga Santander" checked>La Liga Santander <br>
    <input name="liga" type="radio" value="Premier League">Premier League <br>
    <input name="liga" type="radio" value="Serie A">Serie A <br>
    <input name="liga" type="radio" value="Bundesliga">Bundesliga <br>
    <input name="liga" type="radio" value="Ligue 1">Ligue 1 <br><br>

    <label for="opcion">- Cuál crees que gana el balón de oro?</label>
    <select name="jugador" id="elegir">
        <option value="salah">Mohamed Salah</option>
        <option value="messi">Leo Messi</option>
        <option value="robert">Robert Lewandowski</option>
    </select> <br><br>

    <label for="opcion2">- Cuál equipo crees que gane la Champions?</label>
    <select name="equipo" id="elegir2">
        <option  value="liverpool">Liverpool</option>
        <option  value="chelsea">Chelsea</option>
        <option  value="city">Manchester City</option>
        <option  value="madrid">Real Madrid</option>
        <option  value="bayern">Bayern Müchen</option>
    </select> <br><br>

    <label for="tipo">- Escoge un color de fondo:</label> <br>
    <input name="color" type="radio" value="verde" checked>Verde <br>
    <input name="color" type="radio" value="amarillo">Amarillo <br>
    <input name="color" type="radio" value="rojo">Rojo <br>
    <input name="color" type="radio" value="azul">Azul <br><br>

    <label for="partidos">- Cuantos partidos ves de fútbol a la semana?</label>
    <input name="partidos" type="number" value="0" min="0"> <br><br>

    <label for="posicion">- En qué posición te gusta jugar?</label> <br>
    <input name="portero" type="checkbox" value="portero">Portero <br>
    <input name="defensa" type="checkbox" value="defensa">Defensa <br>
    <input name="mediocentro" type="checkbox" value="mediocentro">Mediocentro <br>
    <input name="delantero" type="checkbox" value="delantero">Delantero <br><br>

    <label for="puntuar">- Puntúa!!</label>
    <input name="puntuar" type="number" value="0" min="0" max="5"> <br><br>

    <label for="comentario">- Comenta tu valoración</label>
    <input name="comentario" type="textarea" value="" placeholder="Escribe lo que quieras"> <br><br>

    <input type="submit">

</form>